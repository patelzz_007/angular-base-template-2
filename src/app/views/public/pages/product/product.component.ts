import { PublicMutator } from './../../../../services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {
  products: any;

  constructor( private content:PublicMutator) {

    this.products = this.content.getProducts();

    console.log(this.products.product.data.items);
  }
  ngOnInit() {
  }

}
