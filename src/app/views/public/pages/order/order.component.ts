import { ParticlesConfig } from 'src/assets/lib/data/particles-config';
import { PublicMutator } from '../../../../../app/services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

declare var particlesJS: any;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.sass']
})
export class OrderComponent implements OnInit {
  
  contents: any;

  constructor(private content:PublicMutator) { 

    this.contents = this.content.getContents();
    console.log(this.contents);
    
  }
  ngOnInit() {
    this.invokeParticles();
    // particlesJS.load('particles-js', 'assets/data/particles.json', function () { console.log('callback - particles.js config loaded'); });
  }
  
  public invokeParticles(): void {
    // console.log(ParticlesConfig)
    particlesJS('particles', ParticlesConfig, function() {
      console.log('callback - particles.js config loaded and particlas stary');
    });
    
  }
}
