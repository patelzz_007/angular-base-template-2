import { PublicMutator } from './../../../../../../services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.sass']
})
export class ServiceComponent implements OnInit {

  contents: any;

  constructor(private content: PublicMutator) {

    this.contents = this.content.getContents();
    console.log(this.contents.home.service.data.items);

  }

  ngOnInit() {
  }

  // icon(index){
  //   return "fa " + this.contents.home.service.data.items[index].icon + " fa-stack-1x fa-inverse"
  // }

}