import { PublicMutator } from './../../../../../../services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  contents: any;

  constructor( private content:PublicMutator) {

    this.contents = this.content.getContents();
    console.log(this.contents);

   }

  ngOnInit() {
  }

}
