import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceComponent } from './service/service.component';
import { ProductsComponent } from './products/products.component';
import { ContactComponent } from './contact/contact.component';
import { BannerComponent } from './banner/banner.component'



@NgModule({
  declarations: [
    BannerComponent,
    ServiceComponent,
    ProductsComponent,
    ContactComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    BannerComponent,
    ServiceComponent,
    ProductsComponent,
    ContactComponent
  ]
})
export class SectionsModule { }
