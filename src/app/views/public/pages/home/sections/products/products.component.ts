import { PublicMutator } from './../../../../../../services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {

  products: any;

  constructor( private content:PublicMutator) {

    this.products = this.content.getProducts();

    if(this.products.length > 3){
      this.products.splice(3);
      // console.log(this.products.splice(3));
    }

    console.log(this.products.product.data.items);

   }
  ngOnInit() {
  }

}
