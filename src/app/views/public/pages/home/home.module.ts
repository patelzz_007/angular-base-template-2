import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { SectionsModule } from './sections/sections.module';
// import { ParticlesModule } from 'angular-particle';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    SectionsModule,
    CommonModule,
    // ParticlesModule,
    RouterModule.forChild(routes)
  ]
})
export class HomeModule { }
