import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';
import { WidgetsModule } from './widgets/widgets.module';


const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children:[
      { path: '', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)},
      { path: 'order', loadChildren: () => import('./pages/order/order.module').then(m => m.OrderModule)},
      { path: 'product', loadChildren: () => import('./pages/product/product.module').then(m => m.ProductModule)}
    ]
  },
];

@NgModule({
  declarations: [PublicComponent],
  imports: [
    WidgetsModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PublicModule { }
