import { PublicMutator } from './../../../../services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'public-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  contents: any;

  constructor(private content: PublicMutator) {

    this.contents = this.content.getContents();
    // console.log(this.contents.home.service.data.items);

  }

  ngOnInit() {
  }

}
