
import { PublicMutator } from './../../../../services/mutator/public/public.mutator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'public-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  contents: any;

  constructor( private content:PublicMutator ) { 

    this.contents = this.content.getContents();

  }

  ngOnInit() {
  }

}
