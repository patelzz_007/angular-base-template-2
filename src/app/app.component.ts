import { Component } from '@angular/core';
import { PublicApi } from './services/api/public/public.api';
import { PublicMutator } from './services/mutator/public/public.mutator';


declare var particlesJS: any

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Base-Template';
  constructor(
    private api: PublicApi,
    private mutator: PublicMutator,
  ) {
  }


  
  initializeApp() {
    const splashScreen: HTMLElement = document.getElementById('splashScreen');
    return new Promise((resolve) => {
      this.getConfigs((bool) => {
        splashScreen.remove();
        resolve(bool);
      })
    });
  }

  getConfigs(callback) {
    this.api.getConfigs().subscribe((configs) => {
      this.mutator.setConfigs(configs);
      this.getProducts(callback);
    });
  }

  getProducts(callback) {
    this.api.getProducts().subscribe((products) => {
      this.mutator.setProducts(products);
      this.getContents(callback);
    });
  }

  getContents(callback) {
    this.api.getContents().subscribe((contents) => {
      this.mutator.setContents(contents);
      callback(true);
    });

  }
}

export function init(app: AppComponent) {
  return () => app.initializeApp();
}
