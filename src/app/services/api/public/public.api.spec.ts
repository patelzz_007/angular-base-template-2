import { TestBed } from '@angular/core/testing';

import { PublicApi } from './public.api';

describe('PublicApi', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublicApi = TestBed.get(PublicApi);
    expect(service).toBeTruthy();
  });
});
