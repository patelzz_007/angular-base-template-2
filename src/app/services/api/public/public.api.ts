import { Injectable } from '@angular/core';
import { Api } from '../api';

@Injectable({
  providedIn: 'root'
})
export class PublicApi {

  constructor(
    private api: Api,
  ) { }

  getConfigs = () => {
    return this.api.get("assets/configs.json");
  }

  getProducts = () => {
    return this.api.get("assets/products.json");
  }

  getContents = () => {
    return this.api.get("assets/contents.json");
  }

}
