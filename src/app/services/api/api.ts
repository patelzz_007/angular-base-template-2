import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class Api {

  constructor(
    private http: HttpClient
  ) { }

  get = (URI) =>{
    return this.http.get(URI);
  }

  post = (URI, body) =>{
    return this.http.post(URI, body);
  }

  put = (URI, body) =>{
    return this.http.put(URI, body);
  }

  delete = (URI) =>{
    return this.http.delete(URI);
  }

}
