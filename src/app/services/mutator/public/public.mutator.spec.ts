import { TestBed } from '@angular/core/testing';

import { PublicMutator } from './public.mutator';

describe('PublicMutator', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublicMutator = TestBed.get(PublicMutator);
    expect(service).toBeTruthy();
  });
});
