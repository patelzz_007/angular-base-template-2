import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PublicMutator {
  private products:any;
  private configs:any;
  private contents:any;
  constructor() { }

  getSite = () => {
    return {configs: this.configs, contents: this.contents, products: this.products};
  }

  getProducts = () => {
    return this.products;
  }

  setProducts = (products) => {
    this.products = products;
  }

  getConfigs = () => {
    return this.configs;
  }

  setConfigs = (configs) => {
    this.configs = configs;
  }

  getContents = () => {
    return this.contents;
  }

  setContents = (contents) => {
    this.contents = contents;
  }
}
